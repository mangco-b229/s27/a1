Activity

Create sample documents following the models we have created for users and courses for our booking-system.

user1 and user2 are the ids for the user documents.
course1 and course2 are the ids for the course documents.

user1 is enrolled in course1.
user2 is enrolled in course1 and course2.

2 Model Booking System with Embedding

user {

	id - unique identifier for the document,
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments: [
		{

			id - document identifier,
			courseId - the unique identifier for the course,
			courseName - optional
			dateEnrolled
		}
	]

}


course {

	id - unique for the document
	name,
	description,
	price,
	instructor,
	isActive,
	enrollees: [

		{
			id - document identifier,
			userId,
			fullName(optional),
			dateEnrolled
		}

	]

}